#include <iostream>
#include <math.h>

void nultocke(float b,float c, float rj[2]) {
    float d = (b*b) - (4*c);

    if(d > 0) {
        rj[0] = (-b + sqrt(d))/(2);
        rj[1] = (-b - sqrt(d))/(2);
    } else if (d == 0) {
        rj[0] = -b/(2);
        rj[1] = -b/(2);
    } else {
        rj[0] = FP_NAN;
        rj[1] = FP_NAN;
    }
}

float explicitCalc(float a0, float a1, float l1, float l2, int i) {

    //racunanje nultocki
    float nt[2];
    nultocke(-l1,-l2,nt);

    if (nt[0] == 0 && nt[1] == 0) {
        return 0;
    } else if (nt[0] == nt[1]) {
        return pow(nt[0],float(i))*(a0) + float(i)*pow(nt[0],float(i))*((a1/nt[0])-a0);
    } else if (nt[0] != nt[1]) {
        return pow(nt[0],float(i))*(a0-(a1-a0*nt[0])/(nt[1]-nt[0])) + pow(nt[1],float(i))*(a1-a0*nt[0])/(nt[1]-nt[0]);
    } else {
        throw "Diskriminanta manja od nule!";
    }

}

float recursiveCalc(float a0, float a1, float l1, float l2, int i) {

    float next = l1*a1 +  l2*a0;
    float prev = a1;

    if(i <= 2) return next;

    return recursiveCalc(prev,next,l1,l2,i-1);
}

int main() {

    float l1,l2,a0,a1;
    int n;

    std::cout << "Unesite prvi koeficijent λ_1 rekurzivne relacije: ";
    std::cin >> l1;

    std::cout << "Unesite drugo koeficijent λ_2 rekurzivne relacije: ";
    std::cin >> l2;

    std::cout << "Unesite vrijednost nultog clana niza a_0: ";
    std::cin >> a0;

    std::cout << "Unesite vrijednost prvog clana niza a_1: ";
    std::cin >> a1;

    std::cout << "Unesite redni broj n trazenog clana niza: ";
    std::cin >> n;

    std::cout << "Vrijednost n-tog clana niza iz rekurzije: " << recursiveCalc(a0,a1,l1,l2,n) << std::endl;
    std::cout << "Vrijednost n-tog clana niza pomocu formule: " << explicitCalc(a0,a1,l1,l2,n);

    return 0;
}

